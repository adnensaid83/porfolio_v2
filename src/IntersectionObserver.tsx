import { HtmlIcon } from "./assets/HtmlIcon";
import { CssIcon } from "./assets/CssIcon";
import { ReactIcon } from "./assets/ReactIcon";
import { SassIcon } from "./assets/SassIcon";
import { TypescriptIcon } from "./assets/TypescriptIcon";
import { SymfonyIcon } from "./assets/SymfonyIcon";
import { WordpressIcon } from "./assets/WordpressIcon";
import { GithubIcon } from "./assets/GithubIcon";
import { LinkedinIcon } from "./LinkedinIcon";
import { useEffect, useRef, useState } from "react";
import { ArrowUpRight } from "./assets/ArrowUpRight";
import thunderImg from "./assets/thunder express.png";
import webifyImg from "./assets/webify.png";
const sections = [
  {
    name: "À PROPOS",
    path: "a-propos",
  },
  {
    name: "EXPÉRIENCES",
    path: "experiences",
  },
  {
    name: "PORTFOLIO",
    path: "portfolio",
  },
];
const stacks = [
  <HtmlIcon />,
  <CssIcon />,
  <SassIcon />,
  <TypescriptIcon />,
  <ReactIcon />,
  <SymfonyIcon />,
  <WordpressIcon />,
];
const parcoursData = [
  {
    compagny: "Thunder express",
    periode: "Janvier 2023 - Aôut2023",
    poste: "Développeur Front-end",
    description:
      "Mon rôle principal consistait à assurer l'intégration des maquettes et le développement de la partie frontend du site web en utilisant l’ API fournie par l'équipe de développement backend. Cette initiative visait à offrir une expérience utilisateur cohérente entre le site web et l'application mobile, qui fonctionne sous la plateforme Ionic.",
    skill: [
      "React",
      "Typescript",
      "Vite",
      "Redux",
      "Bootstrap",
      "Figma",
      "Api Rest",
      "Sass",
      "Postman",
      "Photoshop",
    ],
    details:
      "Thunder express est un projet de livraison de repas à domicile qui compte plus de 10 000 clients. L'équipe de développement de Thunder Express est composée de deux développeurs front-end, deux développeurs backend, un designer, un product owner et un intégrateur.L'équipe opérationnelle se compose de trois membres responsables de la gestion des commandes, soutenus par une équipe de livreurs.J’ai eu l’occasion d’ observer le fonctionnement de l’équipe chargée de la gestion des commandes et  la coordination des livreurs de l’application mobile ce qui m'a permis de mieux comprendre le processus opérationnel et développer une compréhension du secteur de la livraison.",
    qualite: "AUTONOME ET ESPRIT D'ÉQUIPE",
    url: "https://thunder-express.com/",
  },
  {
    compagny: "Webify",
    periode: "Juin 2022 - Aôut2023",
    poste: "Développeur Fullstack junior",
    description:
      "En tant que développeur Fullstack, j'ai commencé mon parcours sur ce projet en tant que stagiaire, où ma mission principale était l'intégration web. Par la suite, lors de mon embauche, j'ai poursuivi mon travail sur ce projet en prenant en charge la conception de la base de données, la mise en place d'API Platform pour créer une API RESTful, ainsi que le développement frontend et la revue de code.",
    skill: [
      "React",
      "Typescript",
      "Redux",
      "Styled components",
      "Sass",
      "Symfony",
      "Api Platform",
      "Photoshop",
    ],
    details: "",
    qualite: "PERSÉVERANT ET AUTONOME",
    url: "https://webify.pro/home",
  },
  {
    compagny: "Bati Azur Reno",
    periode: "Juillet 2022 - Septembre 2022",
    poste: "Développeur Wordpress",
    description:
      "J’ai travaillé en collaboration avec le designer et le spécialiste SEO pour créer le site vitrine de l’entreprise j’avais pour mission: Création d'un thème wordpress sur mesure. Utilisation de Elementor pour l’Intégration web et le Responsive design. Optimisation SEO.",
    skill: [
      "Wordpress",
      "Sass",
      "Responsive design",
      "Mobile first",
      "Photoshop",
    ],
    details:
      "Pendant le projet WordPress, j'ai conçu un thème sur mesure en utilisant Hello Elementor comme base. Pour garantir une personnalisation optimale et maintenir les mises à jour futures, j'ai créé un thème enfant qui hérite de la structure du thème de base. Ensuite, j'ai ajouté mon propre style et mes fonctionnalités spécifiques pour répondre aux besoins du projet.",
    qualite: "RIGOUREUX ET PRÉCIS",
    url: "https://hotellerie-renovation.com/",
  },
  {
    compagny: "Webify",
    periode: "Septembre 2021 - Novembre 2021",
    poste: "Stagiare",
    description:
      "Pendant ma période de stage chez Webify, j'ai eu l'opportunité précieuse de plonger dans le monde de la conception et du développement web, en me concentrant principalement sur l'intégration web dans un projet React. Cette expérience de stage a été le point de départ de mon parcours et au cours de cette période, j’ai eu l’occasion d'acquérir une compréhension approfondie du cycle de développement web et d'apprendre à collaborer efficacement au sein d'une équipe",
    skill: [
      "React",
      "Bootstrap",
      "Sass",
      "Responsive design",
      "Mobile first",
      "Photoshop",
    ],
    details: "",
    qualite: "Curieux",
    url: "https://webify.pro/home",
  },
  {
    compagny: "Wild Code School",
    logo: "",
    periode: "Novembre 2021 - Février 2022",
    poste: "Développeur Web et Mobile",
    description:
      "Cette formation mettait l'accent sur la pratique et la collaboration. Nous avons travaillé sur plusieurs projets concrets, ce qui m'a permis d'acquérir une expérience pratique et de développer mes compétences en tant que développeur web.",
    skill: [
      "PHP",
      "Symfony",
      "SCRUM",
      "Bootstrap",
      "Sass",
      "Responsive design",
      "Mobile First",
      "Mysql",
    ],
    details: "",
    qualite: "Curieux et Travail d'équipe",
    url: "https://www.wildcodeschool.com/",
  },
  {
    compagny: "SFR - SCELEC",
    logo: "SFR",
    periode: "Janvier 2019 - Décembre 2020",
    poste: "Coresponsable technique",
    description:
      "Coresponsable technique d’une équipe de 20 techniciens. Gestion des SAV et des réclamations",
    skill: [],
    details: "",
    qualite: "Rigoureux et Esprit d'équipe",
    url: "https://www.sfr.fr/",
  },
  {
    compagny: "SFR - SCELEC",
    logo: "SFR",
    periode: "Janvier 2014 - Décembre 2020",
    poste: "Téchnicien réseaux",
    description:
      "Installer, vérifier et dépanner les équipements sur site. Remise des systèmes en état de fonctionnement.",
    skill: [],
    details: "Élus meilleur technicien de la région Rhône-Alpes",
    qualite: "Gestion du temps et du stress",
    url: "https://www.sfr.fr/",
  },
  {
    compagny: "Université de Gênes",
    logo: "genes",
    periode: "Septembre 2009 - Décembre 2012",
    poste: "DUT en Informatique",
    description:
      "Diplôme de 3 ans en ingénierie informatique. Programmation orientée objet administration base de données systèmes d'exploitation et réseaux",
    skill: [],
    details: "",
    qualite: "Autonome et Capacité à apprendre rapidement",
    url: "https://unige.it/en",
  },
];
function App() {
  const [activeSection, setActiveSection] = useState<string | null>(null);
  const observer = useRef<IntersectionObserver | null>(null);

  useEffect(() => {
    //create new instance and pass a callback function
    observer.current = new IntersectionObserver((entries) => {
      const visibleSection = entries.find(
        (entry) => entry.isIntersecting
      )?.target;
      if (visibleSection) {
        setActiveSection(visibleSection.id);
      }
    });

    //Get custom attribute data-section from all sections
    const sections = document.querySelectorAll("[data-section]");

    sections.forEach((section) => {
      observer.current?.observe(section);
    });
    //Cleanup function to remove observer
    return () => {
      sections.forEach((section) => {
        observer.current?.unobserve(section);
      });
    };
  }, []);

  return (
    <div className="text-white min-h-screen bg-blue100 backdrop-blur-sm lg:bg-blue">
      <div className="max-w-[992px] mx-auto">
        <div className="lg:flex lg:justify-between lg:gap-4">
          <header className="py-12 lg:sticky lg:top-0 lg:flex lg:max-h-screen lg:w-1/2 lg:flex-col lg:justify-between lg:py-24">
            <div className="px-6 lg:px-0">
              <h1 className="font-bold pb-2 text-4xl lg:text-5xl">
                Adnen Said
              </h1>
              <h2 className="pb-4 text-l lg:text-xl">Développeur Fullstack</h2>
              <p className="pb-6 font-light max-w-sm leading-relaxed text-gray">
                Je conçois des fonctionnalités web personnalisées adaptées aux
                besoins uniques de chaque projet.
              </p>
              {/*                 <h3 className="flex gap-4 items-center pr-0 text-sm text-gray font-bold lg:pr-6">
                  Tech Stack
                  <span className="inline-block w-[1px] h-[15px] bg-gray"></span>
                </h3> */}
              <ul className="flex gap-2">
                {stacks.map((s, i) => (
                  <li key={i} className="text-gray hover:text-white">
                    {s}
                  </li>
                ))}
              </ul>
              <nav className="hidden max-w-xs lg:block">
                <ul className="mt-24 text-xs tracking-widest font-bold">
                  {sections.map((s, i) => (
                    <li
                      key={i}
                      className={`group transition-all flex items-center gap-4 py-3 hover:ease-out hover:text-white ${
                        activeSection === s.path ? "text-white" : "text-gray"
                      } `}
                    >
                      <span
                        className={`transition-all inline-block w-[30px] h-[1px] group-hover:ease-out group-hover:w-[60px] group-hover:bg-white ${
                          activeSection === s.path
                            ? "item-enter bg-white"
                            : "bg-gray"
                        }`}
                      ></span>
                      <a href={`#${s.path}`}> {s.name} </a>
                    </li>
                  ))}
                </ul>
              </nav>
            </div>
            <div className="flex flex-col gap-8 px-6 lg:px-0">
              <div className="pt-6 flex flex-col gap-3 lg:pt-0 lg:flex-row"></div>
              <ul className="flex items-center gap-4">
                <li className="text-gray hover:text-white">
                  <GithubIcon />
                </li>
                <li className="text-gray hover:text-white">
                  <LinkedinIcon />
                </li>
                <li>
                  <address className="not-italic font-light max-w-sm leading-relaxed text-gray hover:underline">
                    <a href="mailto:web@adnensaid.fr">web@adnensaid.fr</a>
                  </address>
                </li>
              </ul>
            </div>
          </header>
          <div className="text-gray lg:w-1/2 lg:py-24 lg:px-0">
            <Apropos />
            <Experiences />
            <Portfolio />
          </div>
        </div>
      </div>
    </div>
  );
}

function Apropos() {
  return (
    <section data-section className="pb-12 lg:pb-24" id="a-propos">
      <div className="sticky top-0 lg:hidden">
        <h4 className="mb-12 py-6 px-6 text-sm font-bold tracking-widest text-white backdrop-blur-sm bg-blue200 lg:bg-blue">
          À PROPOS
        </h4>
      </div>
      <div className="px-6 lg:px-0">
        <p className="leading-relaxed font-light ">
          En 2021, lors de mon stage chez{" "}
          <span className="text-white">Webify</span> j'ai eu l'opportunité
          d'explorer le domaine de la conception et du développement web. Cette
          expérience a été{" "}
          <span className="text-white"> le point de départ</span> de mon
          parcours et au cours de cette période, j’ai eu l’occasion d'acquérir
          une vision globale du cycle de
          <span className="text-white"> développement d'un projet</span> en me
          concentrant principalement sur l'
          <span className="text-white">intégration web</span> dans la création
          du site web de l'entreprise en{" "}
          <span className="text-white">React</span>.
          <br />
          <br />
          Après avoir valider une formation de{" "}
          <span className="text-white">développeur web et mobile</span> à la{" "}
          <span className="text-white"> Wild Code School </span>
          j'ai continué à travailler chez Webify et durant cette expérience j'ai
          contribué à la création du site internet de l'application mobile{" "}
          <span className="text-white">Thunder Express</span>, qui compte plus
          de 15 000 clients. De plus, j'ai développé un thème sur mesure pour le
          projet WordPress <span className="text-white">Bati Azure Reno</span>,
          démontrant ma polyvalence et ma capacité à concevoir des solutions
          adaptées aux besoins spécifiques de chaque projet. <br />
          En parallèle, j'ai également créé une fonctionnalité de demande de
          devis personnalisable sur{" "}
          <span className="text-white"> le site Webify</span>, permettant aux
          clients de choisir parmi 4 packs et de générer une liste de questions
          adaptées à leur choix, simplifiant ainsi le processus de demande.
          <br />
          J'ai une passion pour le <span className="text-white"> vélo</span> et
          j'apprécie jouer aux <span className="text-white">échecs</span>.
        </p>
      </div>
    </section>
  );
}

function Experiences() {
  const [showDetails, setShowDetails] = useState(
    Array(parcoursData.length).fill(false)
  );
  return (
    <section data-section className="pb-12 lg:pb-24" id="experiences">
      <div className="sticky top-0 lg:hidden">
        <h4 className="mb-12 py-6 px-6 text-sm font-bold tracking-widest text-white backdrop-blur-sm bg-blue200 lg:bg-blue">
          EXPÉRIENCES
        </h4>
      </div>
      <ol className="flex flex-col gap-12 px-6 lg:px-0">
        {parcoursData.map((p, i: number) => (
          <li
            key={i}
            className="cursor-pointer transition-all rounded-md group/item lg:hover:bg-blueHover lg:hover:shadow-[inset_-10_1px_0_0_rgba(148,163,184,0.1)]"
          >
            <div className="md:grid md:grid-cols-8 md:gap-16 lg:gap-8 lg:px-2 lg:py-3">
              <header className="text-xs font-semibold uppercase my-2 md:col-span-2">
                {p.periode}
              </header>
              <div className="md:col-span-6">
                <a target="_blank" href={p.url}>
                  <div className="text-white flex items-center gap-2 mb-2 text-sm lg:group-hover/item:text-green">
                    {p.poste} . {p.compagny}
                    <button className="group block w-[12px] lg:group-hover/item:ml-1 lg:group-hover/item:mb-1">
                      <ArrowUpRight />
                    </button>
                  </div>
                </a>
                <ul className="flex gap-4 text-xs font-semibold">
                  <li
                    className={`cursor-pointer border-green ${
                      !showDetails[i] && "border-b-[1px] text-white"
                    }`}
                    onClick={() =>
                      setShowDetails((prevState) =>
                        prevState.map((value, index) =>
                          index === i ? !value : value
                        )
                      )
                    }
                  >
                    Description
                  </li>
                  <li
                    className={`cursor-pointer border-green ${
                      showDetails[i] && "border-b-[1px] text-white"
                    }`}
                    onClick={() =>
                      setShowDetails((prevState) =>
                        prevState.map((value, index) =>
                          index === i ? !value : value
                        )
                      )
                    }
                  >
                    Détails
                  </li>
                </ul>
                {!showDetails[i] ? (
                  <p className="text-sm leading-normal font-light my-2">
                    {p.description}
                  </p>
                ) : (
                  <p className="text-sm leading-normal font-light my-2">
                    {p.details}
                  </p>
                )}
                <ul className="flex flex-wrap">
                  {p.skill.map((s, j) => (
                    <li
                      key={j}
                      className="mr-1.5 mt-2 bg-green100 text-green text-xs font-medium rounded-full px-2 py-1"
                    >
                      {s}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </li>
        ))}
      </ol>
    </section>
  );
}

const projects = [
  {
    name: "Thunder express",
    image: thunderImg,
    description:
      "J'ai assuré l'intégration des maquettes et le développement de la partie frontend du site web en utilisant l’ API fournie par l'équipe de développement backend",
  },
  {
    name: "Webify",
    image: webifyImg,
    description:
      "Développer le site professionnel de l'entreprise en React v17 et Symfony 6 api platform",
  },
];

function Portfolio() {
  return (
    <section data-section className="pb-12 lg:pb-24" id="portfolio">
      <div className="sticky top-0 lg:hidden">
        <h4 className="mb-12 py-6 px-6 text-sm font-bold tracking-widest text-white backdrop-blur-sm bg-blue200 lg:bg-blue">
          PROJETS
        </h4>
      </div>
      <ul className="">
        {projects.map((p, i) => (
          <li
            key={i}
            className="grid grid-cols-8 mb-8 gap-4 group/item cursor-pointer transition-all rounded-md group/item lg:hover:bg-blueHover lg:hover:shadow-[inset_-10_1px_0_0_rgba(148,163,184,0.1)] lg:px-2 lg:py-3"
          >
            <img
              src={p.image}
              alt="img"
              className="rounded border-2 border-gray transition col-span-2"
              loading="lazy"
            />
            <div className="col-span-6">
              <h4 className="flex items-center gap-2 mb-2 text-sm text-white lg:group-hover/item:text-green">
                {p.name}
                <button className="group block w-[12px] lg:group-hover/item:ml-1 lg:group-hover/item:mb-1">
                  <ArrowUpRight />
                </button>
              </h4>
              <p className="text-sm leading-normal font-light my-2">
                {p.description}
              </p>
            </div>
          </li>
        ))}
      </ul>
    </section>
  );
}

export default App;
