import { HtmlIcon } from "./assets/HtmlIcon";
import { CssIcon } from "./assets/CssIcon";
import { ReactIcon } from "./assets/ReactIcon";
import { SassIcon } from "./assets/SassIcon";
import { TypescriptIcon } from "./assets/TypescriptIcon";
import { SymfonyIcon } from "./assets/SymfonyIcon";
import { WordpressIcon } from "./assets/WordpressIcon";
import { GithubIcon } from "./assets/GithubIcon";
import { LinkedinIcon } from "./LinkedinIcon";
import { MouseEvent, ReactNode, useEffect, useRef, useState } from "react";
import thunderImg from "./assets/thunder express.png";
import webifyImg from "./assets/webify.png";

const parcoursData = [
  {
    compagny: "Thunder express",
    periode: "Janvier 2023 - Aôut2023",
    poste: "Développeur Front-end",
    description:
      "Mon rôle principal consistait à assurer l'intégration des maquettes et le développement de la partie frontend du site web en utilisant l’ API fournie par l'équipe de développement backend. Cette initiative visait à offrir une expérience utilisateur cohérente entre le site web et l'application mobile, qui fonctionne sous la plateforme Ionic.",
    skill: [
      "React",
      "Typescript",
      "Vite",
      "Redux",
      "Bootstrap",
      "Figma",
      "Api Rest",
      "Sass",
      "Postman",
      "Photoshop",
    ],
    details:
      "Thunder express est un projet de livraison de repas à domicile qui compte plus de 10 000 clients. L'équipe de développement de Thunder Express est composée de deux développeurs front-end, deux développeurs backend, un designer, un product owner et un intégrateur.L'équipe opérationnelle se compose de trois membres responsables de la gestion des commandes, soutenus par une équipe de livreurs.J’ai eu l’occasion d’ observer le fonctionnement de l’équipe chargée de la gestion des commandes et  la coordination des livreurs de l’application mobile ce qui m'a permis de mieux comprendre le processus opérationnel et développer une compréhension du secteur de la livraison.",
    qualite: "AUTONOME ET ESPRIT D'ÉQUIPE",
    url: "https://thunder-express.com/",
  },
  {
    compagny: "Webify",
    periode: "Juin 2022 - Aôut2023",
    poste: "Développeur Fullstack junior",
    description:
      "En tant que développeur Fullstack, j'ai commencé mon parcours sur ce projet en tant que stagiaire, où ma mission principale était l'intégration web. Par la suite, lors de mon embauche, j'ai poursuivi mon travail sur ce projet en prenant en charge la conception de la base de données, la mise en place d'API Platform pour créer une API RESTful, ainsi que le développement frontend et la revue de code.",
    skill: [
      "React",
      "Typescript",
      "Redux",
      "Styled components",
      "Sass",
      "Symfony",
      "Api Platform",
      "Photoshop",
    ],
    details: "",
    qualite: "PERSÉVERANT ET AUTONOME",
    url: "https://webify.pro/home",
  },
  {
    compagny: "Bati Azur Reno",
    periode: "Juillet 2022 - Septembre 2022",
    poste: "Développeur Wordpress",
    description:
      "J’ai travaillé en collaboration avec le designer et le spécialiste SEO pour créer le site vitrine de l’entreprise j’avais pour mission: Création d'un thème wordpress sur mesure. Utilisation de Elementor pour l’Intégration web et le Responsive design. Optimisation SEO.",
    skill: [
      "Wordpress",
      "Sass",
      "Responsive design",
      "Mobile first",
      "Photoshop",
    ],
    details:
      "Pendant le projet WordPress, j'ai conçu un thème sur mesure en utilisant Hello Elementor comme base. Pour garantir une personnalisation optimale et maintenir les mises à jour futures, j'ai créé un thème enfant qui hérite de la structure du thème de base. Ensuite, j'ai ajouté mon propre style et mes fonctionnalités spécifiques pour répondre aux besoins du projet.",
    qualite: "RIGOUREUX ET PRÉCIS",
    url: "https://webify.pro/home",
  },
  {
    compagny: "Webify",
    periode: "Septembre 2021 - Novembre 2021",
    poste: "Stagiare",
    description:
      "Pendant ma période de stage chez Webify, j'ai eu l'opportunité précieuse de plonger dans le monde de la conception et du développement web, en me concentrant principalement sur l'intégration web dans un projet React. Cette expérience de stage a été le point de départ de mon parcours et au cours de cette période, j’ai eu l’occasion d'acquérir une compréhension approfondie du cycle de développement web et d'apprendre à collaborer efficacement au sein d'une équipe",
    skill: [
      "React",
      "Bootstrap",
      "Sass",
      "Responsive design",
      "Mobile first",
      "Photoshop",
    ],
    details: "",
    qualite: "Curieux",
    url: "https://webify.pro/home",
  },
  {
    compagny: "Wild Code School",
    logo: "",
    periode: "Novembre 2021 - Février 2022",
    poste: "Développeur Web et Mobile",
    description:
      "Cette formation mettait l'accent sur la pratique et la collaboration. Nous avons travaillé sur plusieurs projets concrets, ce qui m'a permis d'acquérir une expérience pratique et de développer mes compétences en tant que développeur web.",
    skill: [
      "PHP",
      "Symfony",
      "SCRUM",
      "Bootstrap",
      "Sass",
      "Responsive design",
      "Mobile First",
      "Mysql",
    ],
    details: "",
    qualite: "Curieux et Travail d'équipe",
    url: "https://www.wildcodeschool.com/",
  },
  {
    compagny: "SFR - SCELEC",
    logo: "SFR",
    periode: "Janvier 2019 - Décembre 2020",
    poste: "Coresponsable technique",
    description:
      "Coresponsable technique d’une équipe de 20 techniciens. Gestion des SAV et des réclamations",
    skill: [],
    details: "",
    qualite: "Rigoureux et Esprit d'équipe",
    url: "https://www.sfr.fr/",
  },
  {
    compagny: "SFR - SCELEC",
    logo: "SFR",
    periode: "Janvier 2014 - Décembre 2020",
    poste: "Téchnicien réseaux",
    description:
      "Installer, vérifier et dépanner les équipements sur site. Remise des systèmes en état de fonctionnement.",
    skill: [],
    details: "Élus meilleur technicien de la région Rhône-Alpes",
    qualite: "Gestion du temps et du stress",
    url: "https://www.sfr.fr/",
  },
  {
    compagny: "Université de Gênes",
    logo: "genes",
    periode: "Septembre 2009 - Décembre 2012",
    poste: "DUT en Informatique",
    description:
      "Diplôme de 3 ans en ingénierie informatique. Programmation orientée objet administration base de données systèmes d'exploitation et réseaux",
    skill: [],
    details: "",
    qualite: "Autonome et Capacité à apprendre rapidement",
    url: "https://unige.it/en",
  },
];
const sections = [
  {
    name: "À PROPOS",
    path: "a-propos",
  },
  {
    name: "EXPÉRIENCES",
    path: "experiences",
  },
  {
    name: "PORTFOLIO",
    path: "portfolio",
  },
];
const stacks = [
  <HtmlIcon />,
  <CssIcon />,
  <SassIcon />,
  <TypescriptIcon />,
  <ReactIcon />,
  <SymfonyIcon />,
  <WordpressIcon />,
];
const projects = [
  {
    name: "Thunder express",
    image: thunderImg,
    description:
      "J'ai assuré l'intégration des maquettes et le développement de la partie frontend du site web en utilisant l’ API fournie par l'équipe de développement backend",
    skill: [
      "React",
      "Typescript",
      "Vite",
      "Redux",
      "Bootstrap",
      "Figma",
      "Api Rest",
      "Sass",
      "Postman",
      "Photoshop",
    ],
    url: "https://thunder-express.com/",
  },
  {
    name: "Webify",
    image: webifyImg,
    description:
      "Développer le site professionnel de l'entreprise en React v17 et Symfony 6 api platform",
    skill: [
      "React",
      "Typescript",
      "Redux",
      "Styled components",
      "Sass",
      "Symfony",
      "Api Platform",
      "Photoshop",
    ],
    url: "https://webify.pro/home",
  },
];
function App() {
  const [activeSection, setActiveSection] = useState<string | null>(null);
  const sectionsZone = useRef<NodeListOf<HTMLElement> | []>([]);

  const handleScroll = () => {
    const pageYOffset = window.scrollY;
    let newActiveSection = null;

    sectionsZone.current.forEach((section: any) => {
      const sectionOffsetTop = section.offsetTop;
      const sectionHeight = section.offsetHeight;

      if (
        pageYOffset >= sectionOffsetTop &&
        pageYOffset < sectionOffsetTop + sectionHeight
      ) {
        newActiveSection = section.id;
      }
    });

    setActiveSection(newActiveSection);
  };

  useEffect(() => {
    sectionsZone.current = document.querySelectorAll("[data-section]");
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <SplitScreen leftWeight="w-1/2" rightWeight="w-1/2">
      <>
        <div>
          <h1 className="font-bold pb-2 text-4xl text-white lg:text-5xl">
            Adnen Said
          </h1>
          <h2 className="pb-4 text-l text-white lg:text-xl">
            Développeur Fullstack
          </h2>
          <p className="pb-6 font-light max-w-sm leading-relaxed text-gray">
            Je conçois des fonctionnalités web personnalisées adaptées aux
            besoins uniques de chaque projet.
          </p>
          <ul className="flex gap-2">
            {stacks.map((s, i) => (
              <li key={i} className="text-gray hover:text-white">
                {s}
              </li>
            ))}
          </ul>
          <nav className="hidden max-w-[200px] lg:block">
            <ul className="mt-24 text-xs tracking-widest font-bold">
              {sections.map((s, i) => (
                <li
                  key={i}
                  className={`group transition-all flex items-center gap-4 py-3 hover:ease-out hover:text-white ${
                    activeSection === s.path ? "text-white" : "text-gray"
                  } `}
                >
                  <span
                    className={`transition-all inline-block w-[30px] h-[1px] group-hover:ease-out group-hover:w-[60px] group-hover:bg-white ${
                      activeSection === s.path
                        ? "item-enter bg-white"
                        : "bg-gray"
                    }`}
                  ></span>
                  <a href={`#${s.path}`}> {s.name} </a>
                </li>
              ))}
            </ul>
          </nav>
        </div>
        <div className="flex flex-col gap-8 ml-1">
          <div className="pt-6 flex flex-col gap-3 lg:pt-0 lg:flex-row"></div>
          <ul className="flex items-center gap-4">
            <li className="text-gray hover:text-white">
              <GithubIcon />
            </li>
            <li className="text-gray hover:text-white">
              <LinkedinIcon />
            </li>
            <li>
              <address className="not-italic font-light max-w-sm leading-relaxed text-gray hover:underline">
                <a href="mailto:web@adnensaid.fr">web@adnensaid.fr</a>
              </address>
            </li>
          </ul>
        </div>
      </>
      <>
        <Apropos />
        <Experiences />
        <Portfolio />
      </>
    </SplitScreen>
  );
}

type SplitScreenProps = {
  children: [ReactNode, ReactNode];
  leftWeight?: string;
  rightWeight?: string;
};
function SplitScreen({
  children,
  leftWeight = "w-1/2",
  rightWeight = "w-1/2",
}: SplitScreenProps) {
  let [mousePosition, setMousePosition] = useState({ x: 0, y: 0 });
  const [left, right] = children;

  function handleMouseMove({ clientX, clientY, currentTarget }: MouseEvent) {
    let { left, top } = currentTarget?.getBoundingClientRect();
    let xPosition = clientX - left;
    let yPosition = clientY - top;
    setMousePosition({ x: xPosition, y: yPosition });
  }
  return (
    <div className="relative text-gray" onMouseMove={handleMouseMove}>
      <div
        className="pointer-events-none fixed inset-0 z-30 transition duration-300 lg:absolute"
        style={{
          background: `radial-gradient(600px at ${mousePosition.x}px ${mousePosition.y}px, rgba(29, 78, 216, 0.15), transparent 80%)`,
        }}
      ></div>
      <div className="mx-auto min-h-screen max-w-screen-xl px-6 py-12 font-sans md:px-12 md:py-20 lg:px-24 lg:py-0">
        <div className="lg:flex lg:justify-between lg:gap-4">
          <header
            className={`lg:sticky lg:top-0 lg:flex lg:max-h-screen lg:${leftWeight} lg:flex-col lg:justify-between lg:py-24`}
          >
            {left}
          </header>
          <main className={`pt-24 lg:${rightWeight} lg:py-0`}>{right}</main>
        </div>
      </div>
    </div>
  );
}

function Apropos() {
  return (
    <section
      data-section
      className="mb-16 scroll-mt-16 md:mb-24 lg:mb-36 lg:pt-24 lg:scroll-mt-24"
      id="a-propos"
    >
      <div className="sticky top-0 z-20 -mx-6 mb-4 w-screen bg-blue200 px-6 py-5 backdrop-blur md:-mx-12 md:px-12 lg:sr-only lg:relative lg:top-auto lg:mx-auto lg:w-full lg:px-0 lg:py-0 lg:opacity-0">
        <h4 className="text-sm font-bold uppercase tracking-widest text-white lg:sr-only">
          À PROPOS
        </h4>
      </div>
      <div>
        <p className="leading-relaxed font-light ">
          En 2021, lors de mon stage chez{" "}
          <span className="text-white">Webify</span> j'ai eu l'opportunité
          d'explorer le domaine de la conception et du développement web. Cette
          expérience a été{" "}
          <span className="text-white"> le point de départ</span> de mon
          parcours et au cours de cette période, j’ai eu l’occasion d'acquérir
          une vision globale du cycle de
          <span className="text-white"> développement d'un projet</span> en me
          concentrant principalement sur l'
          <span className="text-white">intégration web</span> dans la création
          du site web de l'entreprise en{" "}
          <span className="text-white">React</span>.
          <br />
          <br />
          Après avoir valider une formation de{" "}
          <span className="text-white">développeur web et mobile</span> à la{" "}
          <span className="text-white"> Wild Code School </span>
          j'ai continué à travailler chez Webify et durant cette expérience j'ai
          contribué à la création du site internet de l'application mobile{" "}
          <span className="text-white">Thunder Express</span>, qui compte plus
          de 15 000 clients. De plus, j'ai développé un thème sur mesure pour le
          projet WordPress <span className="text-white">Bati Azure Reno</span>,
          démontrant ma polyvalence et ma capacité à concevoir des solutions
          adaptées aux besoins spécifiques de chaque projet. <br />
          En parallèle, j'ai également créé une fonctionnalité de demande de
          devis personnalisable sur{" "}
          <span className="text-white"> le site Webify</span>, permettant aux
          clients de choisir parmi 4 packs et de générer une liste de questions
          adaptées à leur choix, simplifiant ainsi le processus de demande.
          <br />
          J'ai une passion pour le <span className="text-white"> vélo</span> et
          j'apprécie jouer aux <span className="text-white">échecs</span>.
        </p>
      </div>
    </section>
  );
}

function Experiences() {
  const [showDetails, setShowDetails] = useState(
    Array(parcoursData.length).fill(false)
  );
  return (
    <section
      data-section
      className="mb-16 scroll-mt-16 md:mb-24 lg:mb-36 lg:scroll-mt-24"
      id="experiences"
    >
      <div className="sticky top-0 z-20 -mx-6 mb-4 w-screen bg-blue200 px-6 py-5 backdrop-blur md:-mx-12 md:px-12 lg:sr-only lg:relative lg:top-auto lg:mx-auto lg:w-full lg:px-0 lg:py-0 lg:opacity-0">
        <h4 className="text-sm font-bold uppercase tracking-widest text-white lg:sr-only">
          EXPÉRIENCES
        </h4>
      </div>
      <ul className="group/list">
        {parcoursData.map((p, i: number) => (
          <li key={i} className="mb-12">
            <div className="group relative grid pb-1 transition-all sm:grid-cols-8 sm:gap-8 md:gap-4 lg:hover:!opacity-100 lg:group-hover/list:opacity-50">
              <div className="hidden absolute -inset-x-4 -inset-y-4 z-0 rounded-md transition motion-reduce:transition-none lg:-inset-x-6 lg:block lg:group-hover:bg-slate/50 lg:group-hover:shadow-[inset_0_1px_0_0_rgba(148,163,184,0.1)] lg:group-hover:drop-shadow-lg"></div>
              <header
                className="z-10 mb-2 mt-1 text-xs font-semibold uppercase tracking-wide text-slate-500 sm:col-span-2"
                aria-label={p.periode}
              >
                {p.periode}
              </header>
              <div className="z-10 sm:col-span-6">
                <h3 className="font-medium leading-snug text-white">
                  <div className="relative">
                    <a
                      className="inline-flex items-baseline font-medium leading-tight text-slate-200 group-hover:text-green focus-visible:text-green group/link text-base"
                      href={p.url}
                      target="_blank"
                    >
                      <span>
                        {p.poste} ·{" "}
                        <span className="inline-block">
                          {p.compagny}
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                            className="inline-block h-4 w-4 shrink-0 transition-transform group-hover:-translate-y-1 group-hover:translate-x-1 group-focus-visible:-translate-y-1 group-focus-visible:translate-x-1 motion-reduce:transition-none ml-1 translate-y-px"
                            aria-hidden="true"
                          >
                            <path
                              fillRule="evenodd"
                              d="M5.22 14.78a.75.75 0 001.06 0l7.22-7.22v5.69a.75.75 0 001.5 0v-7.5a.75.75 0 00-.75-.75h-7.5a.75.75 0 000 1.5h5.69l-7.22 7.22a.75.75 0 000 1.06z"
                              clipRule="evenodd"
                            ></path>
                          </svg>
                        </span>
                      </span>
                    </a>
                  </div>
                </h3>
                <ul className="mt-2 flex gap-4 text-xs font-light">
                  <li
                    className={`cursor-pointer border-green ${
                      !showDetails[i] && "border-b-[1px] text-white"
                    }`}
                    onClick={(e) => {
                      e.preventDefault();
                      setShowDetails((prevState) =>
                        prevState.map((value, index) =>
                          index === i ? !value : value
                        )
                      );
                    }}
                  >
                    Description
                  </li>
                  <li
                    className={`cursor-pointer border-green ${
                      showDetails[i] && "border-b-[1px] text-white"
                    }`}
                    onClick={() =>
                      setShowDetails((prevState) =>
                        prevState.map((value, index) =>
                          index === i ? !value : value
                        )
                      )
                    }
                  >
                    Détails
                  </li>
                </ul>
                {!showDetails[i] ? (
                  <p className="mt-2 text-sm leading-normal font-light">
                    {p.description}
                  </p>
                ) : (
                  <p className="mt-2 text-sm leading-normal font-light">
                    {p.details}
                  </p>
                )}
                <ul
                  className="mt-2 flex flex-wrap"
                  aria-label="Technologies utilisées"
                >
                  {p.skill.map((s, j) => (
                    <li key={j} className="mr-1.5 mt-2">
                      <div className="flex items-center rounded-full bg-green100/10 px-3 py-1 text-xs font-medium leading-5 text-green ">
                        {s}
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </section>
  );
}

function Portfolio() {
  return (
    <section
      data-section
      className="min-h-screen mb-16 scroll-mt-16 md:mb-24 lg:mb-36 lg:scroll-mt-24"
      id="portfolio"
    >
      <div className="sticky top-0 z-20 -mx-6 mb-4 w-screen bg-blue200 px-6 py-5 backdrop-blur md:-mx-12 md:px-12 lg:sr-only lg:relative lg:top-auto lg:mx-auto lg:w-full lg:px-0 lg:py-0 lg:opacity-0">
        <h4 className="text-sm font-bold uppercase tracking-widest text-white lg:sr-only">
          PROJETS
        </h4>
      </div>
      <ul className="group/list">
        {projects.map((p, i) => (
          <li key={i} className="mb-12">
            <div className="group relative grid gap-4 pb-1 transition-all sm:grid-cols-8 sm:gap-8 md:gap-4 lg:hover:!opacity-100 lg:group-hover/list:opacity-50">
              <div className="absolute -inset-x-4 -inset-y-4 z-0 hidden rounded-md transition motion-reduce:transition-none lg:-inset-x-6 lg:block lg:group-hover:bg-slate lg:group-hover:shadow-[inset_0_1px_0_0_rgba(148,163,184,0.1)] lg:group-hover:drop-shadow-lg"></div>
              <div className="z-10 sm:order-2 sm:col-span-6">
                <h3 className="flex items-center gap-2 mb-2 text-sm text-white lg:group-hover/item:text-green">
                  <a
                    className="inline-flex items-baseline font-medium leading-tight text-white hover:text-green focus-visible:text-green group/link text-base"
                    href=""
                    target="_blank"
                    aria-label={p.name}
                  >
                    <span className="absolute -inset-x-4 -inset-y-2.5 hidden rounded md:-inset-x-6 md:-inset-y-4 lg:block"></span>
                    <span className="">
                      {p.name}
                      <span className="inline-block">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          className="inline-block h-4 w-4 shrink-0 transition-transform group-hover/link:-translate-y-1 group-hover/link:translate-x-1 group-focus-visible/link:-translate-y-1 group-focus-visible/link:translate-x-1 motion-reduce:transition-none ml-1 translate-y-px"
                          aria-hidden="true"
                        >
                          <path
                            fillRule="evenodd"
                            d="M5.22 14.78a.75.75 0 001.06 0l7.22-7.22v5.69a.75.75 0 001.5 0v-7.5a.75.75 0 00-.75-.75h-7.5a.75.75 0 000 1.5h5.69l-7.22 7.22a.75.75 0 000 1.06z"
                            clipRule="evenodd"
                          ></path>
                        </svg>
                      </span>
                    </span>
                  </a>
                </h3>
                <p className="mt-2 text-sm leading-normal font-light my-2">
                  {p.description}
                </p>
                <ul
                  className="mt-2 flex flex-wrap"
                  aria-label="Technologies utilisées"
                >
                  {p.skill.map((s, j) => (
                    <li key={j} className="mr-1.5 mt-2">
                      <div className="flex items-center rounded-full bg-green100/10 px-3 py-1 text-xs font-medium leading-5 text-green ">
                        {s}
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
              <img
                src={p.image}
                alt="img"
                className="rounded border-2 border-white/10 transition group-hover:border-white/30 sm:order-1 sm:col-span-2 sm:translate-y-1"
                loading="lazy"
              />
            </div>
          </li>
        ))}
      </ul>
    </section>
  );
}

export default App;

/* 
              <h3 className="flex gap-4 items-center pr-0 text-sm text-gray font-bold lg:pr-6">
                Tech Stack
                <span className="inline-block w-[1px] h-[15px] bg-gray"></span>
              </h3>
*/
