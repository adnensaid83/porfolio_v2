/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    colors: {
      transparent: "transparent",
      blue: "#0e1526",
      blue100: "#101e3f",
      blue200: "rgba(13, 21, 39, .8)",
      blueHover: "rgb(15 23 42)",
      white: "#E2E8F0",
      gray: "#94A3B8",
      green: "#5EEAD4",
      green100: "#2DD4BF1A",
      slate: "#16213C",
    },
    screens: {
      sm: "550px",
      md: "768px",
      lg: "992px",
      xl: "1262px",
    },
    fontFamily: {
      sans: ["Inter", "sans-serif"],
    },
    extend: {},
  },
  plugins: [],
};
